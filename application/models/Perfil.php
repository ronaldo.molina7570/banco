<?php
class Perfil extends CI_Model{
  //funcion para insertar nuevo perfil
        public function insertar($datosPerfil){
            return $this->db->insert('perfil',$datosPerfil);
        }
        //FUNCION PARA OBTENER LOS DATOS DE LA BD
        public function obtener_datos(){
            $query = $this->db->get('perfil');
            if($query->num_rows()>0){
                return $query; //cuadno la base de datos si tiene datos
            }else{
                return false; //cuando no hay registro en la base de datos
            }
        }
        //FUNCION PARA OBTENER LOS DATOS POR ID DE LA BD
        public function obtener_datos_Id($id_usuario){
          $this->db->where("fk_id_usuario",$id_usuario);//filtrando de acuerdo al usuario conectado
          $query=$this->db->get('perfil');
          if ($query->num_rows()>0){
            return $query; //cuando SI hay registros en la bdd
          }else {
            return false; //cuando NO hay registros en la bdd
          }
        }
        //funcion para eliminar datos
        public function eliminar_perfil($id_perfil ){
            $this->db->where("id_perfil ",$id_perfil );
            return $this->db->delete("perfil");
        }
        //funcion para obtener 1 solo dato
        public function actualizar_datos($id_perfil){
          $this->db->where("id_perfil",$id_perfil);
          $query = $this->db->get('perfil');
          if($query->num_rows()>0){
              //echo "resulado: ";
              //print_r($query->row());
              return $query->row(); //retorna solo 1 fila del registro
          }else{
              //echo "error";
              return false; //cuando no hay registro en la base de datos
          }
      }
      //actualizar
        public function actualizar($id_perfil,$datos_perfil){
            $this->db->where("id_perfil",$id_perfil);
            return $this->db->update('perfil',$datos_perfil);

        }
        public function consultarNombre($nombre_perfil){
         $this->db->where('nombre_perfil',$nombre_perfil);
         $query=$this->db->get('perfil');
         if($query->num_rows()>0){
           return $query->row();
         }else{
           false;
         }
       }
}

?>
