<?php
class Cuenta extends CI_Model{
  //FUNCION PARA OBTENER LOS DATOS DE LA BD
  public function obtener_datos($id_cliente){
      $this->db->where("id_cliente",$id_cliente);
      $query = $this->db->get('cliente');
      if($query->num_rows()>0){
          return $query; //cuadno la base de datos si tiene datos
      }else{
          return false; //cuando no hay registro en la base de datos
      }

}
//funcion para obtener 1 solo dato
public function retiro($id_cliente,$retiro){
  $this->db->set('cliente','dinero_cliente-$retiro');
  $this->db->where("id_cliente",$id_cliente);
  $query = $this->db->get('cliente');
  if($query->num_rows()>0){
      //echo "resulado: ";
      //print_r($query->row());
      return $query->row(); //retorna solo 1 fila del registro
  }else{
      //echo "error";
      return false; //cuando no hay registro en la base de datos
  }
}
public function actualizar($id_cliente,$retiro){

    $this->db->set('dinero_cliente',$retiro-'dinero_cliente');
    $this->db->where("id_cliente",$id_cliente);
    return $this->db->update('cliente');

}
public function actualizar_dinero($id_cliente,$deposito){

    $this->db->set('dinero_cliente',$deposito);
    $this->db->where("id_cliente",$id_cliente);
    return $this->db->update('cliente');

}
}
?>
