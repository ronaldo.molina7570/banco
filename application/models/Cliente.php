<?php
class Cliente extends CI_Model{
  //funcion para insertar nuevo usuario
        public function insertar($datosCliente){
            return $this->db->insert('cliente',$datosCliente);
        }
        //FUNCION PARA OBTENER LOS DATOS DE LA BD
        public function obtener_datos(){
            $query = $this->db->get('cliente');
            if($query->num_rows()>0){
                return $query; //cuadno la base de datos si tiene datos
            }else{
                return false; //cuando no hay registro en la base de datos
            }
        }
        //funcion para eliminar datos
        public function eliminar_cliente($id_cliente ){
            $this->db->where("id_cliente ",$id_cliente );
            return $this->db->delete("cliente");
        }
        //funcion para obtener 1 solo dato
        public function actualizar_datos($id_cliente){
          $this->db->where("id_cliente",$id_cliente);
          $query = $this->db->get('cliente');
          if($query->num_rows()>0){
              //echo "resulado: ";
              //print_r($query->row());
              return $query->row(); //retorna solo 1 fila del registro
          }else{
              //echo "error";
              return false; //cuando no hay registro en la base de datos
          }
      }
      //actualizar
        public function actualizar($id_cliente,$datos_cliente){
            $this->db->where("id_cliente",$id_cliente);
            return $this->db->update('cliente',$datos_cliente);

        }
        //funcion para buscar el mismo numero de cedula
        public function consultarClientePorCedula($cedula_cliente){
          $this->db->where('cedula_cliente',$cedula_cliente);
          $query=$this->db->get('cliente');
          if($query->num_rows()>0){
            return $query->row();
          }else{
            false;
          }
        }
}


 ?>
