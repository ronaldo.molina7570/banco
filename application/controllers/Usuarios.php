<?php
class Usuarios extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('Usuario');//cargar el modelo de Usuarios
    $this->load->model('perfil');//cargar el modelo de perfiles
    //verificar si existe o no alguien conectado


  }
  public function datos_usuario(){
    $data["listado_perfiles"]=$this->perfil->obtener_datos();
    $this->load->view('encabezado');
    $this->load->view('Usuarios/datos_usuario',$data);
    $this->load->view('pie');
  }
  public function tablaUsuarios(){
    $idUsuarioConectado=$this->session->userdata("usuario_Conectado")["id"];
    $data["listado_usuarios"]=$this->Usuario->obtener_datos_Id($idUsuarioConectado);
    $this->load->view('Usuarios/tablaUsuarios',$data);
  }
  public function actualizar_usuarios($id_usuario){
    $data["editar_usuario"]=$this->Usuario->actualizar_datos($id_usuario);
    $data["listado_perfiles"]=$this->perfil->obtener_datos();
    $this->load->view('encabezado');
    $this->load->view('Usuarios/actualizar_usuarios',$data);
    $this->load->view('pie');
  }
  //funcion para gauradar los datos del cliente
      public function guardarUsuario(){
               $cedula=$this->input->post('cedula_usuario');
               $nombres=$this->input->post('nombre_usuario');
               $apellidos=$this->input->post('apellidos_usuario');
               $perfil=$this->input->post('perfil_usario');
               $correo=$this->input->post('correo_usuario');
               $pass=$this->input->post('pass_usuario');
               //armado arreglo para ingreso de datos en la bd
               $datosNuevoUsuario=array
               (
                   'cedula_usuario'=>$cedula,
                   'nombre_usuario'=>$nombres,
                   'apellidos_usuario'=>$apellidos,
                   'perfil_usario'=>$perfil,
                   'correo_usuario'=>$correo,
                   'pass_usuario'=>$pass,
                   'fk_id_usuario'=>$this->session->userdata("usuario_Conectado")["id"]
               );
               if($this->Usuario->insertar($datosNuevoUsuario)){
                  //si es verdadero si se inserto
                  //mensaje flash para confirmar
                  $this->session->set_flashdata("confirmacion1","Datos del cliente guardados correctamente");
                  redirect('usuarios/datos_usuario');

               }else{
                   //no se inserto
                   redirect('usuarios/datos_usuario');
               }

          }
          //funcion para eliminar los datos de la bd
          public function eliminar_usuario($id_usuario){
                  if($this->Usuario->eliminar_usuario($id_usuario)){
                    $this->session->set_flashdata("eliminacion","Datos del cliente actualizados correctamente");
                   redirect('Usuarios/datos_usuario');
                  }else{
                      echo "error al eliminar";
                  }
              }
              //funcion para editar datos del cliente
              public function editar_usaurio(){
                  $id_usuario=$this->input->post("id_usuario");//captura el id del usuario a editar
                  $datosEditadosUsuario=array(
                    "cedula_usuario"=>$this->input->post('cedula_usuario'),
                    "nombre_usuario"=>$this->input->post('nombre_usuario'),
                    "apellidos_usuario"=>$this->input->post('apellidos_usuario'),
                    "correo_usuario"=>$this->input->post('correo_usuario'),
                    "perfil_usario"=>$this->input->post('perfil_usario'),
                    "correo_usuario"=>$this->input->post('correo_usuario'),
                    "pass_usuario"=>$this->input->post('pass_usuario'),
                    );
                    if($this->Usuario->actualizar($id_usuario,$datosEditadosUsuario)){
                        //mensaje flash para confirmar
                        $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
                          redirect('usuarios/datos_usuario');
                        }else{
                          echo "error al actualizar";

                        }
              }
              //funcion para ver si existe el numero de cedula en la bd
              public function validarCedulaExistente(){
                    $cedula_usuario=$this->input->post('cedula_usuario');
                    $UsarioExistente=$this->Usuario->consultarUsuarioPorCedula($cedula_usuario);
                    if($UsarioExistente){
                      echo json_encode(FALSE);
                      //echo("<br> <br>");
                      //print_r($clienteExistente);
                    }else{
                      echo json_encode(TRUE);
                    }
                  }

}

?>
