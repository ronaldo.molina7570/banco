<?php
class Seguridades extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('Seguridad');
  }
  public function login(){
    $this->load->view('encabezado');
    $this->load->view('Seguridades/Login');
    $this->load->view('pie');
  }
  public function autenticarUsuario(){
        $email=$this->input->post('correo_usuario');
        $password=$this->input->post('pass_usuario');
        $usuarioConsultado=$this->Seguridad->consultarPorEmailPassword($email,$password);
        if ($usuarioConsultado) {
          //creando una variable de sesion -> userdata
          $datosSesion=array(
            "id"=>$usuarioConsultado->id_usuario,
            "email"=>$usuarioConsultado->correo_usuario,
            "perfil"=>$usuarioConsultado->perfil_usario
          );//array que contiene los valores de la sesion
            $this->session->set_userdata("usuario_Conectado",$datosSesion);//creando Sesion
            $this->session->set_flashdata("confirmacionUsuario","Acceso exitoso, Bienvenido al sistema");//creando Sesion
            redirect("/");
        }else{
          $this->session->set_flashdata("error","Email o constraseña incorrecto");
          redirect("Seguridades/Login");
        }
      }

      public function autenticarCliente(){
            $email=$this->input->post('correo_cliente');
            $password=$this->input->post('pass_cliente');
            $usuarioConsultado=$this->Seguridad->consultarPasswordCliente($email,$password);
            if ($usuarioConsultado) {
              //creando una variable de sesion -> userdata
              $datosSesion=array(
                "id"=>$usuarioConsultado->id_cliente,
                "email"=>$usuarioConsultado->correo_cliente,
                "perfil"=>$usuarioConsultado->rol_cliente,
                "dinero"=>$usuarioConsultado->dinero_cliente,
                "nombre"=>$usuarioConsultado->nombres_cliente,
                "apellido"=>$usuarioConsultado->apellidos_cliente
                //"monto"=>$usuarioConsultado->rol_cliente
              );//array que contiene los valores de la sesion
                $this->session->set_userdata("usuario_Conectado",$datosSesion);//creando Sesion
                $this->session->set_flashdata("confirmacionUsuario","Acceso exitoso, Bienvenido al sistema");//creando Sesion
                redirect("/");
            }else{
              $this->session->set_flashdata("error","Email o constraseña incorrecto");
              redirect("Seguridades/Login");
            }
          }
      public function cerrarSesion(){
        $this->session->sess_destroy();//borrar todas las sesiones existentes
        redirect("Seguridades/Login");
      }
}

?>
