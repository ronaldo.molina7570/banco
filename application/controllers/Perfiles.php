<?php
class Perfiles extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('perfil');//cargar el modelo de perfiles
    //verificar si existe o no alguien conectado
            if (!$this->session->userdata("usuario_Conectado")) {
              $this->session->set_flashdata("error","Por favor Inicie Sesion");
              redirect("Seguridades/login");
            }else{
              if (!($this->session->userdata("usuario_Conectado")["perfil"]!="ADMINISTRADOR" )){
                redirect("Seguridades/cerrarSesion");
              }
            }
  }
  public function datos_perfiles(){
    $this->load->view('encabezado');
    $this->load->view('Perfiles/datos_perfiles');
    $this->load->view('pie');
  }
  public function tablaPerfiles(){
    $idUsuarioConectado=$this->session->userdata("usuario_Conectado")["id"];
    $data["listado_perfiles"]=$this->perfil->obtener_datos_Id($idUsuarioConectado);
    $this->load->view('Perfiles/tablaPerfiles',$data);
  }
  public function actualizar_perfiles($id_perfil){
    $data["editar_perfil"]=$this->perfil->actualizar_datos($id_perfil);
    $this->load->view('encabezado');
    $this->load->view('Perfiles/actualizar_perfiles',$data);
    $this->load->view('pie');
  }
  //funcion para gauradar los datos de los perfiles
      public function guardarPerfil(){
               $nombre=$this->input->post('nombre_perfil');
               //armado arreglo para ingreso de datos en la bd
               $datosNuevoPerfil=array
               (
                   'nombre_perfil'=>$nombre,
                   'fk_id_usuario'=>$this->session->userdata("usuario_Conectado")["id"]
               );
               if($this->perfil->insertar($datosNuevoPerfil)){
                  //si es verdadero si se inserto
                  //mensaje flash para confirmar
                  $this->session->set_flashdata("confirmacion1","Datos del cliente guardados correctamente");
                  redirect('perfiles/datos_perfiles');

               }else{
                   //no se inserto
                   redirect('perfiles/datos_perfiles');
               }

          }
          //funcion para eliminar los datos de la bd
          public function eliminar_usuario($id_usuario){
                  if($this->Usuario->eliminar_usuario($id_usuario)){
                    $this->session->set_flashdata("eliminacion","Datos del cliente actualizados correctamente");
                   redirect('Usuarios/datos_usuario');
                  }else{
                      echo "error al eliminar";
                  }
              }
              //funcion para editar datos del perfil
              public function editar_perfil(){
                  $id_perfil=$this->input->post("id_perfil");//captura el id del usuario a editar
                  $datosEditadosPerfil=array(
                    "nombre_perfil"=>$this->input->post('nombre_perfil'),
                    );
                    if($this->perfil->actualizar($id_perfil,$datosEditadosPerfil)){
                        //mensaje flash para confirmar
                        $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
                          redirect('perfiles/datos_perfiles');
                        }else{
                          echo "error al actualizar";

                        }
              }
          public function validarNombreExistente(){
                $nombre_perfil=$this->input->post('nombre_perfil');
                $nombreExistente=$this->perfil->consultarNombre($nombre_perfil);
                if($nombreExistente){
                  echo json_encode(FALSE);
                }else{
                  echo json_encode(TRUE);
                }
              }

}

?>
