<?php
class Cuentas extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('Seguridad');
    $this->load->model('Cuenta');
    //$this->load->model('cliente');

  }
  public function menuCuenta($id_cliente = null){
    //$data["editar_dinero"]=$this->Cuenta->actualizar_dinero($id_cliente);
    $id_cliente=$this->input->post("id_cliente");
    $data["listado_cliente"]=$this->Cuenta->obtener_datos($id_cliente);
    $this->load->view('encabezado');
    $this->load->view('cuenta/menuCuenta',$data);
    $this->load->view('pie');
  }
  public function tablaDatos($id_cliente){
    $data["listado_cliente"]=$this->Cuenta->obtener_datos($id_cliente);
    $this->load->view('cuenta/tablaDatos',$data);
  }
  public function retiro($id_cliente){
    $dinero=$this->input->post('retiro');
    $data["editar_dinero"]=$this->Cuenta->retiro($id_cliente,$dinero);
    $this->load->view('encabezado');
    $this->load->view('cuentas/menuCuenta',$data);
    $this->load->view('pie');
  }
  public function editar_dinero(){
      $id_cliente=$this->input->post("id_cliente");//captura el id del usuario a editar
      $retiro=$this->input->post("retiro");
        if($this->Cuenta->actualizar($id_cliente,$retiro)){
            //mensaje flash para confirmar
            $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
              redirect('cuentas/menuCuenta');
            }else{
              echo "error al actualizar";

            }
  }
  public function editar_deposito(){
      $id_cliente=$this->input->post("id_cliente");//captura el id del usuario a editar
      $deposito=$this->input->post("deposito");
        if($this->Cuenta->actualizar_dinero($id_cliente,$deposito)){
            //mensaje flash para confirmar
            $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
              redirect('cuentas/menuCuenta');
            }else{
              echo "error al actualizar";

            }
  }
}

?>
