<?php
class Clientes extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->model('cliente');
    //verificar si existe o no alguien conectado


  }
  //hola
  //carga la vista de los datos de los clientes
  public function datos_clientes(){
    $this->load->view('encabezado');
    $this->load->view('clientes/datos_clientes');
    $this->load->view('pie');
  }
  public function tablaClientes(){
    $data["listado_clientes"]=$this->cliente->obtener_datos();
    $this->load->view('clientes/tablaClientes',$data);
  }

  public  function actualizar_cliente($id_cliente){
    $data["editar_cliente"]=$this->cliente->actualizar_datos($id_cliente);
    $this->load->view('encabezado');
    $this->load->view('clientes/actualizar_cliente',$data);
    $this->load->view('pie');
  }
  //funcion para gauradar los datos del cliente
  public function guardarCliente(){
    $dinero_cliente=0;
    $rol_cliente="CLIENTE";
    $cuenta_aleatoria=rand(1000,10000000);//crear una cuenta aleatoria
    $nombre=$this->input->post('nombres_cliente');
    $apellido=$this->input->post('apellidos_cliente');
    $cedula=$this->input->post('cedula_cliente');
    $correo=$this->input->post('correo_cliente');
    $fecha_nacimiento=$this->input->post('fecha_nacimiento_cliente');
    $estado_civil=$this->input->post('estado_civil_cliente');
    $genero=$this->input->post('genero_cliente');
    $direccion=$this->input->post('direccion_cliente');
    $telefono=$this->input->post('telefono_cliente');
    $celular=$this->input->post('celular_cliente');
    $pass=$this->input->post('pass_cliente');
    //armado arreglo para ingreso de datos en la bd
    $datosNuevoCliente=array
    (
      'nombres_cliente'=>$nombre,
      'apellidos_cliente'=>$apellido,
      'cedula_cliente'=>$cedula,
      'correo_cliente'=>$correo,
      'fecha_nacimiento_cliente'=>$fecha_nacimiento,
      'estado_civil_cliente'=>$estado_civil,
      'genero_cliente'=>$genero,
      'direccion_cliente'=>$direccion,
      'telefono_cliente'=>$telefono,
      'celular_cliente'=>$celular,
      'cuenta_cliente'=>$cuenta_aleatoria,
      'dinero_cliente'=>$dinero_cliente,
      'pass_cliente'=>$pass,
      'rol_cliente'=>$rol_cliente

    );
    if ($this->cliente->insertar($datosNuevoCliente)) {
        $this->session->set_flashdata("confirmacion1","Datos del cliente guardados correctamente");
          redirect('Seguridades/login');
    }else{
      redirect('clientes/datos_clientes');
    }
  }
  //funcion para eliminar los datos de la bd
  public function eliminar_cliente($id_cliente){
          if($this->cliente->eliminar_cliente($id_cliente)){
            $this->session->set_flashdata("eliminacion","Datos del cliente actualizados correctamente");
           redirect('/');
          }else{
              echo "error al eliminar";
          }
      }
  //funcion para editar datos del cliente
  public function editar_cliente(){
      $id_cliente=$this->input->post("id_cliente");//captura el id del usuario a editar
      $datosEditadosClientes=array(
        "cedula_cliente"=>$this->input->post('cedula_cliente'),
        "nombres_cliente"=>$this->input->post('nombres_cliente'),
        "apellidos_cliente"=>$this->input->post('apellidos_cliente'),
        "correo_cliente"=>$this->input->post('correo_cliente'),
        "fecha_nacimiento_cliente"=>$this->input->post('fecha_nacimiento_cliente'),
        "estado_civil_cliente"=>$this->input->post('estado_civil_cliente'),
        "genero_cliente"=>$this->input->post('genero_cliente'),
        "direccion_cliente"=>$this->input->post('direccion_cliente'),
        "telefono_cliente"=>$this->input->post('telefono_cliente'),
        "celular_cliente"=>$this->input->post('celular_cliente'),
        "cuenta_cliente"=>$this->input->post('cuenta_cliente'),
        "dinero_cliente"=>$this->input->post('dinero_cliente'),
        "pass_cliente"=>$this->input->post('pass_cliente'),
        );
        if($this->cliente->actualizar($id_cliente,$datosEditadosClientes)){
            //mensaje flash para confirmar
            $this->session->set_flashdata("actualizacion","Datos del cliente actualizados correctamente");
              redirect('clientes/datos_clientes');
            }else{
              echo "error al actualizar";

            }
  }
  //funcion para ver si existe el numero de cedula en la bd
  public function validarCedulaExistente(){
        $cedula_cliente=$this->input->post('cedula_cliente');
        $clienteExistente=$this->cliente->consultarClientePorCedula($cedula_cliente);
        if($clienteExistente){
          echo json_encode(FALSE);
          //echo("<br> <br>");
          //print_r($clienteExistente);
        }else{
          echo json_encode(TRUE);
        }
      }
}
?>
