<center>
    <h2 style="font-weight:bold;">PERFILES REGISTRADOS</h2>
</center>
<br>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">
                ID
            </th>
            <th class="text-center">
                NOMBRE
            </th>
            <th class="text-center">
                ACCIONES
            </th>
        </tr>
    </thead>
    <tbody>
      <?php if ($listado_perfiles): ?>
        <?php foreach ($listado_perfiles->result() as $perfil_temporal): ?>
        <tr>
                <td class="text-center"><?php echo $perfil_temporal->id_perfil ; ?></td>
                <td class="text-center"><?php echo $perfil_temporal->nombre_perfil; ?></td>
                <td align="center">
                <a href="<?php echo site_url(); ?>/perfiles/actualizar_perfiles/<?php echo $perfil_temporal->id_perfil; ?>">
                  <i class="glyphicon glyphicon-pencil" title="EDITAR"></i>
                </a>
                <a href="<?php echo site_url(); ?>/perfiles/eliminar_perfil/<?php echo $perfil_temporal->id_perfil; ?>"
                  onclick="return confirm('Seguro de desea eliminar?')">
                <i class="glyphicon glyphicon-trash" title="ELIMINAR"></i>
              </a>
                </td>
        </tr>
        <?php endforeach?>
        <?php endif; ?>
    </tbody>
</table>
