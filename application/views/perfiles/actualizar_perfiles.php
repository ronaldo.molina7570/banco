<div class="row">
    <div class="col-md-12 text-center" >
        <legend>
            ACTUALIZAR DATOS DEL PERFIL
        </legend>

    </div>
    <br>
    <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <form class="" action="<?php echo site_url(); ?>/perfiles/editar_perfil" method="post" id="formulario_editar_perfil">
                  <input type="hidden" name="id_perfil" id="id_perfil" class="form-control" value="<?php echo $editar_perfil->id_perfil; ?>" required >
                  <label for="">Nombres:</label>
                  <input type="text" name="nombre_perfil" id="nombre_perfil" class="form-control" value="<?php echo $editar_perfil->nombre_perfil; ?>" required autocomplete="off">
                  <br>
                  <br>
                    <button type="submint" name="button" class="btn btn-primary btn-lg active">
                        ACTUALIZAR
                    </button>
                    <a href="<?php echo site_url()	?>/perfiles/datos_perfiles" class="btn btn-danger btn-lg active" role="button" aria-pressed="true" >CANCELAR</a>
                    <br>
                    <br>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
</div>
<script type="text/javascript">
    	$("#formulario_editar_perfil").validate({
    		rules:{
          nombre_perfil:{
    				required:true,
            remote:{
              url:"<?php echo site_url('perfiles/validarNombreExistente'); ?>",
                    data:{
                      "$nombre_perfil":function(){
                        return $("#nombre_perfil").val();
                      }
                    },
                    type:"post"
            }
    			}
    		},
    		messages:{
          nombre_perfil:{
    				required:"Por favor ingrese el nombre del perfil",
            remote:"El nombre ya existe ingrese otro"
    			}
    		},

    	});

</script>
