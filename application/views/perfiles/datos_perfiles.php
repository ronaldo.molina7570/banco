<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <legend>DATOS DE LOS PERFILES</legend>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar Perfil</button>
        <br>
        <div id="contenedor_listado_perfiles">

        </div>
      </div>
    </div>
    <br>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/perfiles/guardarPerfil" method="post" id="formulario_nuevo_perfil">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>NUEVO PERFIL</b></h4>
      </div>
      <div class="modal-body">
        <p>Formulario de Nuevo Perfil</p>
        <br>
            <label for="">Nombre:</label>
            <input type="text" name="nombre_perfil" id="nombre_perfil" class="form-control" value="" placeholder="Ingrese el nombre del perfil" required  autocomplete="off">
            <br>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <button type="submit" name="button" class="btn btn-success" >Guardar Perfil</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    	$("#formulario_nuevo_perfil").validate({
    		rules:{
          nombre_perfil:{
    				required:true,
            remote:{
              url:"<?php echo site_url('perfiles/validarNombreExistente'); ?>",
                    data:{
                      "$nombre_perfil":function(){
                        return $("#nombre_perfil").val();
                      }
                    },
                    type:"post"
            }
    			}
    		},
    		messages:{
          nombre_perfil:{
    				required:"Por favor ingrese el nombre del perfil",
            remote:"El nombre ya existe ingrese otro"
    			}
    		},

    	});

</script>
<script type="text/javascript">
submitHandler:function(form){
      var url=$(form).prop("action");//capturando url (controlador/funcion)
      //generando peticion asincrona
      $.ajax({
           url:url,//action del formulario
           type:'post',//definiendo el tipo de envio de datos post/get
           data:$(form).serialize(), //enviando los datos ingresados en el formulario
           success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
              //alert('Cliente guardado exitosamente');
              cargarUsuarios();//llamado para actualizar el listado de cleitnes
              $(form)[0].reset();//limpiar capos del formulario
           },
           error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
              alert('Error al insertar, intente nuevamente');
           }
      });
    }
</script>
<script type="text/javascript">

  function cerrarModal(){
    $("#myModal").modal("hide");

  }
</script>
<script type="text/javascript">
function cargarUsuarios(){
  $("#contenedor_listado_perfiles").load('<?php echo site_url("perfiles/tablaPerfiles"); ?>');
}
cargarUsuarios();
</script>
