<!DOCTYPE html>
<html lang="en">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

     <!-- Site Metas -->
    <title>Banco</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>/assets/images/apple-touch-icon.png">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script type="text/javascript" 	src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/style.css">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
    <!-- ALL VERSION CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/versions.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/custom.css">

    <!-- Modernizer for Portfolio -->
    <script src="<?php echo base_url(); ?>/assets/js/modernizer.js"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<style>
.navbar-nav{
	float: right;
}
.error{
	color: red;
	font-weight: normal;
}
input.error{
	border: 1px solid red;
}
</style>
<body class="host_version">

	<!-- Modal -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header tit-up">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Iniciar Sesión</h4>
			</div>
			<div class="modal-body customer-box row">
				<div class="col-md-12">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#Login" data-toggle="tab">Usuario</a></li>
            <li><a href="#LoginCliente" data-toggle="tab">Cliente</a></li>
						<li><a href="#Registration" data-toggle="tab">Registrarse</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
            <!-- Login Usuarios -->
						<div class="tab-pane active" id="Login">
							<form role="form" class="form-horizontal" action="<?php echo site_url(); ?>/Seguridades/autenticarUsuario"  method="post">
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Correo Electrónico:</label>
									<input class="form-control" type="email" name="correo_usuario" id="correo_usuario" placeholder="Ingrese su Correo Electrónico" type="text">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Contraseña:</label>
									<input class="form-control" type="password" name="pass_usuario" id="pass_usuario" placeholder="Ingrese su Contraseña">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-success btn-lg">
										Aceptar
									</button>
                  <button type="button" class="btn btn-danger btn-lg" onclick="cerrarModal();">
										Cancelar
                  </button>
								</div>
							</div>
							</form>
						</div>
            <!-- Login Clientes -->
						<div class="tab-pane " id="LoginCliente">
							<form role="form" class="form-horizontal" action="<?php echo site_url(); ?>/Seguridades/autenticarCliente"  method="post">
                <p>Cliente</p>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Correo Electrónico:</label>
									<input class="form-control" type="email" name="correo_cliente" id="correo_cliente" placeholder="Ingrese su Correo Electrónico" type="text">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Contraseña:</label>
									<input class="form-control" type="password" name="pass_cliente" id="pass_cliente" placeholder="Ingrese su Contraseña">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-success btn-lg" >
										Aceptar
									</button>
                  <button type="button" class="btn btn-danger btn-lg" onclick="cerrarModal();">
										Cancelar
                  </button>
								</div>
							</div>
							</form>
						</div>
            <!-- Registro de clientes -->
						<div class="tab-pane" id="Registration">
							<form  class="" action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="formulario_nuevo_cliente">
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Nombres:</label>
									<input type="text" class="form-control" name="nombres_cliente" id="nombres_cliente" value="" placeholder="Ingrese sus Nombres" type="text" required autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Apellidos:</label>
                  <input type="text" class="form-control" name="apellidos_cliente" id="apellidos_cliente" placeholder="Ingrese sus Apellidos" type="text" required autocomplete="off">								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">N° de cédula:</label>
									<input type="number" class="form-control" name="cedula_cliente" id="cedula_cliente" placeholder="Ingrese su Número de Cedula" type="text" required autocomplete="off">
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Correo Electrónico:</label>
									<input type="email" class="form-control" name="correo_cliente" id="correo_cliente" placeholder="Ingrese su Correo Electrónico" type="text" required autocomplete="off">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
                  <label for="">Fecha de Nacimiento:</label>
									<input type="date" class="form-control" name="fecha_nacimiento_cliente" id="fecha_nacimiento_cliente" placeholder="Ingrese su Fecha de Nacimiento" type="text" required autocomplete="off">
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Estado Civil:</label>
                  <select class="form-control" name="estado_civil_cliente" id="estado_civil_cliente">
                    <option value="Soltero/a">Soltero/a</option>
                    <option value="Casado/a">Casado/a</option>
                    <option value="Viudo/a">Viudo/a</option>
                    <option value="Divorciado/a">Divorciado/a</option>
                  </select>
                </div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Género:</label>
                  <select class="form-control" name="genero_cliente" id="genero_cliente">
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                  </select>
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Direccion exacta:</label>
									<input type="text" class="form-control" name="direccion_cliente" id="direccion_cliente" placeholder="Ingrese su Direccion exacta" type="text" required autocomplete="off">
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Número de Teléfono:</label>
									<input type="number" class="form-control" name="telefono_cliente" id="telefono_cliente" placeholder="Ingrese su Número de Teléfono" type="text"  autocomplete="off">
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Número de Celular:</label>
									<input type="number" class="form-control" name="celular_cliente" id="celular_cliente" placeholder="Ingrese su Número de Celular" type="text" required autocomplete="off">
								</div>
							</div>
              <div class="form-group">
								<div class="col-sm-12">
                  <label for="">Contraseña:</label>
									<input type="password" class="form-control" name="pass_cliente" id="pass_cliente" placeholder="Ingrese su contraseña" type="text" required autocomplete="off">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" name="button" class="btn btn-success btn-lg">
										Guardar &amp; Continuar
									</button>
                  </form>
									<button type="button" class="btn btn-danger btn-lg" onclick="cerrarModal();">
										Cancelar
                  </button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>

    <!-- LOADER -->
	<div id="preloader">
		<div class="loading">
			<div class="finger finger-1">
				<div class="finger-item">
				<span></span><i></i>
				</div>
			</div>
  			<div class="finger finger-2">
				<div class="finger-item">
				<span></span><i></i>
				</div>
			</div>
  			<div class="finger finger-3">
				<div class="finger-item">
				  <span></span><i></i>
				</div>
			</div>
  			<div class="finger finger-4">
				<div class="finger-item">
				<span></span><i></i>
				</div>
			</div>
  			<div class="last-finger">
				<div class="last-finger-item"><i></i></div>
			</div>
		</div>
	</div>
	<!-- END LOADER -->

    <header class="header header_style_01">
        <nav class="megamenu navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logos/banco.png" alt="image"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo site_url() ?>">Inicio</a></li>
                        <?php if ($this->session->userdata("usuario_Conectado")): ?>
                        <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="GERENTE"): ?>
                        <li><a href="<?php echo site_url();?>/Usuarios/datos_usuario">Gestión Usuarios </a></li>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="ADMINISTRADOR"): ?>
                        <li><a href="<?php echo site_url();?>/Clientes/datos_clientes">Gestión Clientes </a></li>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="GERENTE"): ?>
                        <li><a href="<?php echo site_url();?>/Perfiles/datos_perfiles">Gestión Perfiles</a></li>
                        <?php endif; ?>
                        <?php if ($this->session->userdata("usuario_Conectado")["perfil"]=="CLIENTE"): ?>
                        <li ><a href="<?php echo site_url();?>/Cuentas/menuCuenta" <?php echo $this->session->userdata("usuario_Conectado")['nombre']  ?>>Mi Cuenta</button></a></li>
                        <?php endif; ?>
                        <li><a href="<?php echo site_url(); ?>/Seguridades/cerrarSesion">
						            <?php echo $this->session->userdata("usuario_Conectado")['email']  ?>
						            Salir</a>
					               </li>
                        <?php else: ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="btn-light btn-radius btn-brd log" href="#" data-toggle="modal" data-target="#login"><i class="flaticon-padlock"></i> Iniciar Sesión</a></li>
                        </ul>
                        <?php endif; ?>
                    </ul>

                </div>
            </div>
        </nav>
    </header>
<?php if ($this->session->flashdata('confirmacion1')): ?>
  		<script type="text/javascript">

  		Swal.fire({
  			position: 'mid',
  			icon: 'success',
  			title: 'DATOS GUARDADOS CORRECTAMENTE',
  			showConfirmButton: false,
  			timer: 1500
  	})

  		</script>

  	<?php endif; ?>
    <?php if ($this->session->flashdata('error')): ?>
  		<script type="text/javascript">

  		Swal.fire({
  			position: 'mid',
  			icon: 'error',
  			title: 'Email o constraseña incorrecto',
  			showConfirmButton: true,
  			timer: 1500
  	})

  		</script>
  	<?php endif; ?>
    <?php if ($this->session->flashdata('confirmacionUsuario')): ?>
  		<script type="text/javascript">

  		Swal.fire({
  			position: 'mid',
  			icon: 'success',
  			title: 'Acceso exitoso, Bienvenido al sistema',
  			showConfirmButton: false,
  			timer: 1500
  	})

  		</script>

  	<?php endif; ?>

<script type="text/javascript">
    	$("#formulario_nuevo_cliente").validate({
    		rules:{
          nombres_cliente:{
    				required:true
    			},
          apellidos_cliente:{
            required:true
          },
    			cedula_cliente:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10,
            remote:{
                        url:"<?php echo site_url('Clientes/validarCedulaExistente'); ?>",
                        data:{
                          "$cedula_cliente":function(){
                            return $("#cedula_cliente").val();
                          }
                        },
                        type:"post"
                    }
    			},
          correo_cliente:{
            required:true
          },
          fecha_nacimiento_cliente:{
            required:true
          },
          estado_civil_cliente:{
            required:true
          },
          genero_cliente:{
            required:true
          },
    			direccion_cliente:{
    				required:true
    			},
    			celular_cliente:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10
    			}
    		},
    		messages:{
          nombres_cliente:{
    				required:"Por favor ingrese sus nombres"
    			},
          apellidos_cliente:{
            required:"Por favor ingrese sus apellidos"
          },
    			cedula_cliente:{
    				required:"Por favor ingrese su numero de cédula",
    				minlength:"La cedula debe tener minimo 10 digitos",
    				maxlength: "La cedula solo debe tener 10 digitos",
            remote:"La cedula ya existe"
    			},
          correo_cliente:{
            required:"Por favor ingrese sus correo electrónico"
          },
          fecha_nacimiento_cliente:{
            required:"Por favor ingrese sus fecha de nacimiento"
          },
          estado_civil_cliente:{
            required:"Por favor ingrese su estado civil"
          },
          genero_cliente:{
            required:"Por favor ingrese su genero"
          },
    			direccion_cliente:{
    				required:"Por favor ingrese su dirección"
    			},
    			celular_cliente:{
    				required:"Por favor ingrese su numero de celular",
    				minlength:"El numero de celular debe tener minimo 10 digitos",
    				maxlength: "El numero de celular solo debe tener 10 digitos"
    			}
    		},

    	});

</script>
<script type="text/javascript">
submitHandler:function(form){
      var url=$(form).prop("action");//capturando url (controlador/funcion)
      //generando peticion asincrona
      $.ajax({
           url:url,//action del formulario
           type:'post',//definiendo el tipo de envio de datos post/get
           data:$(form).serialize(), //enviando los datos ingresados en el formulario
           success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
              //alert('Cliente guardado exitosamente');
              cargarClientes();//llamado para actualizar el listado de cleitnes
              $(form)[0].reset();//limpiar capos del formulario
           },
           error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
              alert('Error al insertar, intente nuevamente');
           }
      });
    }
</script>
<script type="text/javascript">

  function cerrarModal(){
    $("#login").modal("hide");

  }
</script>
