<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <div class="widget-title">
                        <h3>Pagina actualizado</h3>
                    </div>
                    <p>Pagina destinada a la administracion de los recursos de un banco.</p>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</footer><!-- end footer -->

<div class="copyrights">
    <div class="container">
        <div class="footer-distributed">
            <div class="footer-left">
                <p class="footer-company-name">Ronaldo Molina:</p>
            </div>
        </div>
    </div><!-- end container -->
</div><!-- end copyrights -->
<!-- ALL JS FILES -->
<script src="<?php echo base_url(); ?>/assets/js/all.js"></script>
<!-- ALL PLUGINS -->
<script src="<?php echo base_url(); ?>/assets/js/custom.js"></script>

</body>
</html>
