<div class="row">
    <div class="col-md-12 text-center" >
        <legend>
            ACTUALIZAR DATOS DEL CLIENTE
        </legend>

    </div>
    <br>
    <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <form class="" action="<?php echo site_url(); ?>/Clientes/editar_cliente" method="post" id="formulario_actualizar_cliente">
                  <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $editar_cliente->id_cliente; ?>" required >
                  <label for="">Nombres:</label>
                  <input type="text" name="nombres_cliente" id="nombres_cliente" class="form-control" value="<?php echo $editar_cliente->nombres_cliente; ?>" required>
                  <br>
                  <label for="">Apellidos:</label>
                  <input type="text" name="apellidos_cliente" id="apellidos_cliente" class="form-control" value="<?php echo $editar_cliente->apellidos_cliente; ?>" required>
                  <br>
                  <label for="">N° de cédula:</label>
                  <input type="number" name="cedula_cliente" id="cedula_cliente" class="form-control" value="<?php echo $editar_cliente->cedula_cliente; ?>" required>
                  <br>
                  <label for="">Correo Electrónico:</label>
                  <input type="text" name="correo_cliente" id="correo_cliente" class="form-control" value="<?php echo $editar_cliente->correo_cliente; ?>" required>
                  <br>
                  <label for="">Fecha de Nacimiento:</label>
                  <input type="date" name="fecha_nacimiento_cliente" id="fecha_nacimiento_cliente" class="form-control" value="<?php echo $editar_cliente->fecha_nacimiento_cliente; ?>" required>
                  <br>
                  <label for="">Estado Civil</label>
                  <input type="text" name="estado_civil_cliente" id="estado_civil_cliente" class="form-control" value="<?php echo $editar_cliente->estado_civil_cliente; ?>" required>
                  <br>
                  <label for="">Género:</label>
                  <input type="text" name="genero_cliente" id="genero_cliente" class="form-control" value="<?php echo $editar_cliente->genero_cliente; ?>" required>
                  <br>
                  <label for="">Dirección Exacta:</label>
                  <input type="text" name="direccion_cliente" id="direccion_cliente" class="form-control" value="<?php echo $editar_cliente->direccion_cliente; ?>" required>
                  <br>
                  <label for="">Número de Telefono:</label>
                  <input type="number" name="telefono_cliente" id="telefono_cliente" class="form-control" value="<?php echo $editar_cliente->telefono_cliente; ?>">
                  <br>
                  <label for="">Número de Celular:</label>
                  <input type="number" name="celular_cliente" id="celular_cliente" class="form-control" value="<?php echo $editar_cliente->celular_cliente; ?>" required>
                  <br>
                  <label for="">Número de Cuenta:</label>
                  <input type="number" name="cuenta_cliente" id="cuenta_cliente" class="form-control" value="<?php echo $editar_cliente->cuenta_cliente; ?>" required>
                  <br>
                  <label for="">Dinero:</label>
                  <input type="number" name="dinero_cliente" id="dinero_cliente" class="form-control" value="<?php echo $editar_cliente->dinero_cliente; ?>" required>
                  <br>
                    <br>
                    <button type="submint" name="button" class="btn btn-primary btn-lg active">
                        ACTUALIZAR
                    </button>
                    <a href="<?php echo site_url()	?>/clientes/datos_clientes" class="btn btn-danger btn-lg active" role="button" aria-pressed="true" >CANCELAR</a>
                    <br>
                    <br>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
</div>
<script type="text/javascript">
    	$("#formulario_actualizar_cliente").validate({
    		rules:{
          nombres_cliente:{
    				required:true
    			},
          apellidos_cliente:{
            required:true
          },
    			cedula_cliente:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10,
    			},
          correo_cliente:{
            required:true
          },
          fecha_nacimiento_cliente:{
            required:true
          },
          estado_civil_cliente:{
            required:true
          },
          genero_cliente:{
            required:true
          },
    			direccion_cliente:{
    				required:true
    			},
    			celular_cliente:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10
    			}
    		},
    		messages:{
          nombres_cliente:{
    				required:"Por favor ingrese sus nombres"
    			},
          apellidos_cliente:{
            required:"Por favor ingrese sus apellidos"
          },
    			cedula_cliente:{
    				required:"Por favor ingrese su numero de cédula",
    				minlength:"La cedula debe tener minimo 10 digitos",
    				maxlength: "La cedula solo debe tener 10 digitos",
    			},
          correo_cliente:{
            required:"Por favor ingrese sus correo electrónico"
          },
          fecha_nacimiento_cliente:{
            required:"Por favor ingrese sus fecha de nacimiento"
          },
          estado_civil_cliente:{
            required:"Por favor ingrese su estado civil"
          },
          genero_cliente:{
            required:"Por favor ingrese su genero"
          },
    			direccion_cliente:{
    				required:"Por favor ingrese su dirección"
    			},
    			celular_cliente:{
    				required:"Por favor ingrese su numero de celular",
    				minlength:"El numero de celular debe tener minimo 10 digitos",
    				maxlength: "El numero de celular solo debe tener 10 digitos"
    			}
    		},

    	});

</script>
