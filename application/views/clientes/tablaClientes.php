<center>
    <h2 style="font-weight:bold;">CLIENTES REGISTRADOS</h2>
</center>
<br>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">
                ID
            </th>
            <th class="text-center">
                CEDULA
            </th>
            <th class="text-center">
                NOMBRES
            </th>
            <th class="text-center">
                APELLIDOS
            </th>
            <th class="text-center">
                CORREO ELECTRÓNICO
            </th>
            <th class="text-center">
                FECHA NACIMIENTO
            </th>
            <th class="text-center">
                ESTADO CIVIL
            </th>
            <th class="text-center">
                GÉNERO
            </th>
            <th class="text-center">
                DIRECCIÓN
            </th>
            <th class="text-center">
                TELÉFONO
            </th>
            <th class="text-center">
                CELULAR
            </th>
            <th class="text-center">
                CUENTA
            </th>
            <th class="text-center">
                DINERO
            </th>
            <th class="text-center">
                ACCIONES
            </th>
        </tr>
    </thead>
    <tbody>
      <?php if ($listado_clientes): ?>
        <?php foreach ($listado_clientes->result() as $cliente_temporal): ?>
        <tr>
                <td class="text-center"><?php echo $cliente_temporal->id_cliente ; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->cedula_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->nombres_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->apellidos_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->correo_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->fecha_nacimiento_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->estado_civil_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->genero_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->direccion_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->telefono_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->celular_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->cuenta_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->dinero_cliente; ?></td>
                <td align="center">
                <a href="<?php echo site_url(); ?>/clientes/actualizar_cliente/<?php echo $cliente_temporal->id_cliente; ?>">
                  <i class="glyphicon glyphicon-pencil" title="EDITAR"></i>
                </a>
                <a href="<?php echo site_url(); ?>/clientes/eliminar_cliente/<?php echo $cliente_temporal->id_cliente; ?>"
                  onclick="return confirm('Seguro de desea eliminar?')">
                <i class="glyphicon glyphicon-trash" title="ELIMINAR"></i>
              </a>
                </td>
        </tr>
        <?php endforeach?>
        <?php endif; ?>
    </tbody>
</table>
