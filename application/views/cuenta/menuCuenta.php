<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7">
    <FONT SIZE=5>
    <?php echo $this->session->userdata("usuario_Conectado")['nombre']  ?>
    <?php echo $this->session->userdata("usuario_Conectado")['apellido']  ?>
    </font>
  </div>
</div>
<div class="row">
  <div class="col-md-12" align="center">

    <center>
      <?php echo $this->session->userdata("usuario_Conectado")['email']  ?> 
      <br>

      <FONT color="#0E11D9" SIZE=20 >Saldo:</font>
        <br>
      <FONT SIZE=20>$<?php echo $this->session->userdata("usuario_Conectado")['dinero']  ?></font>
    </center>
  </div>
</div>
<div class="row"align="center">
  <div class="col-md-2">

  </div>
  <div class="col-md-8">

  </div>
  <div class="col-md-2">

  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <center>
      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Transferencia</button>
    </center>
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <center>
      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalCliente">Retiro</button>
    </center>
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <center>
      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalDeposito">Deposito</button>
    </center>
  </div>
</div>
<br>
<br>
<br>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Cuentas/editar_dinero" method="post" id="">
    <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $this->session->userdata("usuario_Conectado")['id']  ?>" required >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Transferencia</b></h4>
      </div>
      <div class="modal-body">
        <br>
            <label for="">Correo Electronico:</label>
            <input type="email" name="cedula_cliente" id="cedula_cliente" class="form-control" value="" placeholder="Ingrese el correo electronico de destino" required  autocomplete="off">
            <br>
            <label for="">Monto:</label>
            <input type="number" name="retiro" id="retiro" class="form-control" value="" placeholder="Ingrese el monto" required  autocomplete="off">
            <br>

      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <button type="submit" name="button" class="btn btn-success">Transferir</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal1();">Cancelar Acción</button>
          </div>
          <div class="col-md-6">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="myModalCliente" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Cuentas/editar_dinero" method="post" id="formulario_nuevo_cliente">
      <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $this->session->userdata("usuario_Conectado")['id']  ?>" required >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Iniciar Sesion Cliente</b></h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="form-group">
          <div class="col-sm-12">
            <label for="">Monto:</label>
            <input class="form-control" type="retiro" name="retiro" id="retiro" placeholder="Ingrese el monto a retirar" type="text" autocomplete="off" required>
          </div>
        </div>
      </div>
      <br>
      <br>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <br>
            <button type="submit" name="button" class="btn btn-success">Aceptar</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="myModalDeposito" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Cuentas/editar_deposito" method="post" id="formulario_nuevo_cliente">
      <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $this->session->userdata("usuario_Conectado")['id']  ?>" required >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Iniciar Sesion Cliente</b></h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="form-group">
          <div class="col-sm-12">
            <label for="">Dinero:</label>
            <input class="form-control" type="deposito" name="deposito" id="deposito" placeholder="Ingrese el monto a depositar" type="text" autocomplete="off" required>
          </div>
        </div>
      </div>
      <br>
      <br>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <br>
            <button type="submit" name="button" class="btn btn-success">Aceptar</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal2();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="myModalDatos" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Cuentas/editar_deposito" method="post" id="formulario_nuevo_cliente">
      <input type="hidden" name="id_cliente" id="id_cliente" class="form-control" value="<?php echo $this->session->userdata("usuario_Conectado")['id']  ?>" required >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Datos Cliente/b></h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="row">
          <div class="col-md-12">
            <br>
            <div id="contenedor_listado_datos">

            </div>
          </div>
        </div>
      </div>
      <br>
      <br>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <br>
            <button type="submit" name="button" class="btn btn-success">Aceptar</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal2();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">

  function cerrarModal1(){
    $("#myModal").modal("hide");

  }
</script>
<script type="text/javascript">

  function cerrarModal(){
    $("#myModalCliente").modal("hide");

  }
</script>
<script type="text/javascript">

  function cerrarModal2(){
    $("#myModalDeposito").modal("hide");

  }
</script>
<script type="text/javascript">
function cargarDatos(){
  $("#contenedor_listado_datos").load('<?php echo site_url("cuentas/tablaDatos"); ?>');
}
cargarDatos();
</script>
