<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">
                CEDULA
            </th>
            <th class="text-center">
                NOMBRES
            </th>
            <th class="text-center">
                APELLIDOS
            </th>
            <th class="text-center">
                CUENTA
            </th>
            <th class="text-center">
                DINERO
            </th>
        </tr>
    </thead>
    <tbody>
      <?php if ($listado_cliente): ?>
        <?php foreach ($listado_cliente->result() as $cliente_temporal): ?>
        <tr>
                <td class="text-center"><?php echo $cliente_temporal->cedula_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->nombres_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->apellidos_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->cuenta_cliente; ?></td>
                <td class="text-center"><?php echo $cliente_temporal->dinero_cliente; ?></td>
        </tr>
        <?php endforeach?>
        <?php endif; ?>
    </tbody>
</table>
