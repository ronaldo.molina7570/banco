<center>
    <h2 style="font-weight:bold;">USUARIOS REGISTRADOS</h2>
</center>
<br>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">
                ID
            </th>
            <th class="text-center">
                CEDULA
            </th>
            <th class="text-center">
                NOMBRES
            </th>
            <th class="text-center">
                APELLIDOS
            </th>
            <th class="text-center">
                PERFIL
            </th>
            <th class="text-center">
                CORREO ELECTRÓNICO
            </th>
            <th class="text-center">
                CONTRASEÑA
            </th>
            <th class="text-center">
                ACCIONES
            </th>
        </tr>
    </thead>
    <tbody>
      <?php if ($listado_usuarios): ?>
        <?php foreach ($listado_usuarios->result() as $usuario_temporal): ?>
        <tr>
                <td class="text-center"><?php echo $usuario_temporal->id_usuario ; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->cedula_usuario; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->nombre_usuario; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->apellidos_usuario; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->perfil_usario; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->correo_usuario; ?></td>
                <td class="text-center"><?php echo $usuario_temporal->pass_usuario; ?></td>
                <td align="center">
                <a href="<?php echo site_url(); ?>/usuarios/actualizar_usuarios/<?php echo $usuario_temporal->id_usuario; ?>">
                  <i class="glyphicon glyphicon-pencil" title="EDITAR"></i>
                </a>
                <a href="<?php echo site_url(); ?>/usuarios/eliminar_usuario/<?php echo $usuario_temporal->id_usuario; ?>"
                  onclick="return confirm('Seguro de desea eliminar?')">
                <i class="glyphicon glyphicon-trash" title="ELIMINAR"></i>
              </a>
                </td>
        </tr>
        <?php endforeach?>
        <?php endif; ?>
    </tbody>
</table>
