<div class="row">
    <div class="col-md-12 text-center" >
        <legend>
            ACTUALIZAR DATOS DEL CLIENTE
        </legend>

    </div>
    <br>
    <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <form class="" action="<?php echo site_url(); ?>/usuarios/editar_usaurio" method="post" id="formulario_actualizar_cliente">
                  <input type="hidden" name="id_usuario" id="id_usuario" class="form-control" value="<?php echo $editar_usuario->id_usuario; ?>" required >
                  <label for="">N° de cédula:</label>
                  <input type="number" name="cedula_usuario" id="cedula_usuario" class="form-control" value="<?php echo $editar_usuario->cedula_usuario; ?>"  required  autocomplete="off">
                  <br>
                  <label for="">Nombres:</label>
                  <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control" value="<?php echo $editar_usuario->nombre_usuario; ?>"  required  autocomplete="off">
                  <br>
                  <label for="">Apellidos:</label>
                  <input type="text" name="apellidos_usuario" id="apellidos_usuario" class="form-control" value="<?php echo $editar_usuario->apellidos_usuario; ?>"  required  autocomplete="off">
                  <br>
                  <label for="">Perfil:</label>
                  <select class="form-control" name="perfil_usario" id="perfil_usario" required>
                                  <option value=""> -- Seleccione --</option>
                                      <?php if ($listado_perfiles): ?>
                                            <?php foreach ($listado_perfiles->result() as $perfil_temporal): ?>
                                                <option value="<?php echo $perfil_temporal->nombre_perfil ; ?>" >
                                                    <?php echo $perfil_temporal -> nombre_perfil; ?>
                                                </option>
                                            <?php endforeach; ?>
                                      <?php endif; ?>
                              </select>
                  <br>
                  <label for="">Correo Electrónico</label>
                  <input type="text" name="correo_usuario" id="correo_usuario" class="form-control" value="<?php echo $editar_usuario->correo_usuario; ?>"  required  autocomplete="off">
                  <br>
                  <label for="">Contraseña</label>
                  <input type="password" name="pass_usuario" id="pass_usuario" class="form-control" value="<?php echo $editar_usuario->pass_usuario; ?>"  required autocomplete="off">
                  <br>
                    <button type="submint" name="button" class="btn btn-primary btn-lg active">
                        ACTUALIZAR
                    </button>
                    <a href="<?php echo site_url()	?>/clientes/datos_clientes" class="btn btn-danger btn-lg active" role="button" aria-pressed="true" >CANCELAR</a>
                    <br>
                    <br>
                    <script type="text/javascript">
                      $("#perfil_usario").val("<?php echo $editar_usuario->perfil_usario; ?>");
                    </script>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
</div>
