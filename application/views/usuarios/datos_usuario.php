<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <legend>DATOS DE LOS USUARIOS</legend>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Agregar Usuario</button>
        <br>
        <div id="contenedor_listado_usuarios">

        </div>
      </div>
    </div>
    <br>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/usuarios/guardarUsuario" method="post" id="formulario_nuevo_usuario">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>NUEVO USUARIO</b></h4>
      </div>
      <div class="modal-body">
        <p>Formulario de Nuevo Usuario</p>
        <br>
            <label for="">N° de cédula:</label>
            <input type="number" name="cedula_usuario" id="cedula_usuario" class="form-control" value="" placeholder="Ingrese el numero de cédula" required  autocomplete="off">
            <br>
            <label for="">Nombres:</label>
            <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control" value="" placeholder="Ingrese los nombres" required  autocomplete="off">
            <br>
            <label for="">Apellidos:</label>
            <input type="text" name="apellidos_usuario" id="apellidos_usuario" class="form-control" value="" placeholder="Ingrese los apellidos" required  autocomplete="off">
            <br>
            <label for="">Perfil:</label>
            <select class="form-control" name="perfil_usario" id="perfil_usario" required>
                              <option value=""> -- Seleccione --</option>
                                  <?php if ($listado_perfiles): ?>
                                        <?php foreach ($listado_perfiles->result() as $perfil_temporal): ?>
                                            <option value="<?php echo $perfil_temporal->nombre_perfil; ?>">
                                              <?php echo $perfil_temporal ->nombre_perfil; ?>
                                            </option>
                                        <?php endforeach; ?>
                                  <?php endif; ?>
                          </select>
            <br>
            <label for="">Correo Electrónico</label>
            <input type="text" name="correo_usuario" id="correo_usuario" class="form-control" value="" placeholder="Ingrese su correo electrónico" required  autocomplete="off">
            <br>
            <label for="">Contraseña</label>
            <input type="password" name="pass_usuario" id="pass_usuario" class="form-control" value="" placeholder="Ingrese una contraseña"   autocomplete="off">
            <br>

      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <button type="submit" name="button" class="btn btn-success" >Guardar Usuario</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    	$("#formulario_nuevo_usuario").validate({
    		rules:{
          nombre_usuario:{
    				required:true
    			},
          apellidos_usuario:{
            required:true
          },
    			cedula_usuario:{
    				required:true,
    				digits:true,
    				minlength:10,
    				maxlength: 10,
            remote:{
                        url:"<?php echo site_url('usuarios/validarCedulaExistente'); ?>",
                        data:{
                          "$cedula_usuario":function(){
                            return $("#cedula_usuario").val();
                          }
                        },
                        type:"post"
                    }
    			},
          perfil_usario:{
            required:true
          },
          correo_usuario:{
            required:true
          },
          pass_usuario:{
            required:true
          }
    		},
    		messages:{
          nombre_usuario:{
    				required:"Por favor ingrese sus nombres"
    			},
          apellidos_usuario:{
            required:"Por favor ingrese sus apellidos"
          },
    			cedula_usuario:{
    				required:"Por favor ingrese su numero de cédula",
    				minlength:"La cedula debe tener minimo 10 digitos",
    				maxlength: "La cedula solo debe tener 10 digitos",
            remote:"La cedula ya existe"
    			},
          perfil_usario:{
            required:"Por favor seleccione un perfil de usuario"
          },
          correo_usuario:{
            required:"Por favor ingrese un correo electrónico"
          },
          pass_usuario:{
            required:"Por favor ingrese una contraseña"
          }
    		},

    	});

</script>
<script type="text/javascript">
submitHandler:function(form){
      var url=$(form).prop("action");//capturando url (controlador/funcion)
      //generando peticion asincrona
      $.ajax({
           url:url,//action del formulario
           type:'post',//definiendo el tipo de envio de datos post/get
           data:$(form).serialize(), //enviando los datos ingresados en el formulario
           success:function(data){ //funcion que se ejecuta cuando SI se realiza la peticion
              //alert('Cliente guardado exitosamente');
              cargarUsuarios();//llamado para actualizar el listado de cleitnes
              $(form)[0].reset();//limpiar capos del formulario
           },
           error:function(data){ //funcion que se ejecuta cuando NO se realiza la peticion
              alert('Error al insertar, intente nuevamente');
           }
      });
    }
</script>
<script type="text/javascript">

  function cerrarModal(){
    $("#myModal").modal("hide");

  }
</script>
<script type="text/javascript">
function cargarUsuarios(){
  $("#contenedor_listado_usuarios").load('<?php echo site_url("usuarios/tablaUsuarios"); ?>');
}
cargarUsuarios();
</script>
