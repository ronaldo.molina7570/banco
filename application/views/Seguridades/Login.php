<br><br>

<br><br>
<div class="row">
  <div class="col-md-6" align="right">
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Usuario</button>
    <br>
  </div>
  <div class="col-md-6" align="left">
    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModalCliente">Cliente</button>
    <br>
    <div id="contenedor_listado_perfiles">
    </div>
  </div>
</div>
<br>
<br>
<br>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Seguridades/autenticarUsuario" method="post" id="formulario_nuevo_cliente">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Iniciar Sesion</b></h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="form-group">
          <div class="col-sm-12">
            <label for="">Correo Electrónico:</label>
            <input class="form-control" type="email" name="correo_usuario" id="correo_usuario" placeholder="Ingrese su Correo Electrónico" type="text" autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <br>
            <label for="">Contraseña:</label>
            <input class="form-control" type="password" name="pass_usuario" id="pass_usuario" placeholder="Ingrese su Contraseña">
          </div>
        </div>
      </div>
      <br>
      <br>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <br>
            <button type="submit" name="button" class="btn btn-success" >Aceptar</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div id="myModalCliente" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
    <form class=""  action="<?php echo site_url(); ?>/Seguridades/autenticarCliente" method="post" id="formulario_nuevo_cliente">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> <b>Iniciar Sesion Cliente</b></h4>
      </div>
      <div class="modal-body">
        <br>
        <div class="form-group">
          <div class="col-sm-12">
            <label for="">Correo Electrónico:</label>
            <input class="form-control" type="email" name="correo_cliente" id="correo_cliente" placeholder="Ingrese su Correo Electrónico" type="text" autocomplete="off">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <br>
            <label for="">Contraseña:</label>
            <input class="form-control" type="password" name="pass_cliente" id="pass_cliente" placeholder="Ingrese su Contraseña">
          </div>
        </div>
      </div>
      <br>
      <br>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <br>
            <button type="submit" name="button" class="btn btn-success" >Aceptar</button>
              </form>
            <button type="button" name="button" class="btn btn-danger"  onclick="cerrarModal();">Cancelar Acción</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  $("#formulario_login").validate({
    rules:{
      correo_usuario:{
        required:true
      },
      pass_usuario:{
        required:true
      }
    },
    messages:{
      correo_usuario:{
        required:"Ingrese el correo"
      },
      pass_usuario:{
        required:"Ingrese su contraseña"
      }
    }
  })
</script>
