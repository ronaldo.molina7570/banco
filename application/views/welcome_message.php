<div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" >
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
    <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
    <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div id="home" class="first-section" style="background-image:url('uploads/slider-01.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
              <div class="big-tagline">
                <h2 data-animation="animated zoomInRight">Sistema <strong>de Banco</strong> .</h2>
                <p class="lead" data-animation="animated fadeInLeft">Sistema para agilizar su tiempo. </p>
              </div>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div><!-- end section -->
    </div>
    <div class="item">
      <div id="home" class="first-section" style="background-image:url('uploads/slider-02.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
              <div class="big-tagline">
                <h2 data-animation="animated zoomInRight">Encontraste el banco <strong>CORECIÓN PR{UEBA </strong></h2>
                <p class="lead" data-animation="animated fadeInLeft">Banco hecho para ti y diseñado par mejorar tu experiencia. </p>
              </div>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div><!-- end section -->
    </div>
    <div class="item">
      <div id="home" class="first-section" style="background-image:url('uploads/slider-03.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
              <div class="big-tagline">
                <h2 data-animation="animated zoomInRight">Las  <strong>mejores</strong> estrategias</h2>
                <p class="lead" data-animation="animated fadeInLeft">Puedes hacer todo movimiento de dinero que tu desees</p>
              </div>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div><!-- end section -->
    </div>
    <!-- Left Control -->
    <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
      <span class="fa fa-angle-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>

    <!-- Right Control -->
    <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
      <span class="fa fa-angle-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
